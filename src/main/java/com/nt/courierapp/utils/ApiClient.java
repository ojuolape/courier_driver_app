package com.nt.courierapp.utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataSource;
import java.io.IOException;
import java.io.InputStream;

public abstract class ApiClient {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final OkHttpClient okHttpClient;

    public ApiClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public Response callForResponse(
            String url,
            String method) {
        try {
//            if (url.startsWith("/")) {
//                url = url.substring(1);
//            }
            Request.Builder req = new Request.Builder()
                    .url(getBaseUrl() + url);
            return getResponse(method, req, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Response callForResponse(
            String url,
            String method,
            DataSource dataSource) {
        try {
//            if (url.startsWith("/")) {
//                url = url.substring(1);
//            }
            Request.Builder req = new Request.Builder()
                    .url(getBaseUrl() + url);
            return getResponse(method, req, dataSource);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Response getResponse(String method, Request.Builder req) {
        return getResponse(method, req, null);
    }

    public Response getResponse(String method, Request.Builder req, DataSource dataSource) {
        try {
            RequestBody requestBody = null;
            if (dataSource != null && dataSource.getInputStream().available() > 0) {
                requestBody = new RequestBody() {

                    @Override
                    public okhttp3.MediaType contentType() {
                        return okhttp3.MediaType.parse(dataSource.getContentType());
                    }

                    @Override
                    public void writeTo(BufferedSink bufferedSink) throws IOException {
                        try (InputStream inputStream = dataSource.getInputStream()) {
                            IOUtils.copy(inputStream, bufferedSink.outputStream());
                        }
                    }
                };
            }
            return okHttpClient.newCall(req.method(method, requestBody).build()).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract String getBaseUrl();
}
