package com.nt.courierapp.utils;

import okhttp3.Response;

import java.util.Optional;

public class ApiCallExceptionForOkHttp extends ApiCallException {

    public ApiCallExceptionForOkHttp(Response response) {
        super(response.request().url().toString(), response.code(), response.message(),
                Optional.ofNullable(response.body())
                        .map(it -> it.contentType())
                        .map(it -> it.toString())
                        .orElse("*/*"));
    }
}
