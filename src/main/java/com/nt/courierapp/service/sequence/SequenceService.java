package com.nt.courierapp.service.sequence;
public interface SequenceService {
    Long getNextId();
}
