package com.nt.courierapp.service;

import com.nt.courierapp.model.DeliveryRequest;
import com.nt.courierapp.model.PasswordResetToken;
import com.nt.courierapp.model.PortalUser;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public interface MailService {

    void sendSimpleMail(String to, String subject, String message);

    void sendEmail(String message, String subject, String recipient);

    String getContentFromTemplate(Map<String, Object> model, String templateName);

    void sendNewDeliveryRequestMail(DeliveryRequest deliveryRequest);

    void sendNewDeliveryRequestMailToAdmin(DeliveryRequest deliveryRequest);

    void sendNewUserMail(PortalUser portalUser, String password);

    void sendPasswordResetEmail(PasswordResetToken passwordResetToken);
}
