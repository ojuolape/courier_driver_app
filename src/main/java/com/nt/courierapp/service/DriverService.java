package com.nt.courierapp.service;

import com.nt.courierapp.message.request.DriverVehicleForm;
import com.nt.courierapp.message.response.DriverDto;
import com.nt.courierapp.model.Driver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface DriverService {
    Driver createDriver(DriverVehicleForm driverVehicleForm);
    Page findBySearchFilter(String filter, Pageable pageable);
    DriverDto toDriverDto(Driver driver);
}
