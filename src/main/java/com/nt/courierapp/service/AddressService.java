package com.nt.courierapp.service;

import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.model.Address;

import java.util.Optional;

public interface AddressService {
    Optional<Address> createAddress(AddressForm addressForm);
}
