package com.nt.courierapp.service;


import com.nt.courierapp.message.request.LoginForm;
import com.nt.courierapp.message.request.TraileLoginResponse;

public interface TraileApiClient {

  TraileLoginResponse login(LoginForm loginForm);
}
