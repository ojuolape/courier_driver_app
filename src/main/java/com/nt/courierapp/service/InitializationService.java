package com.nt.courierapp.service;

import org.springframework.stereotype.Component;


public interface InitializationService {
    void createAdminUser();
    void loadRoles();
}
