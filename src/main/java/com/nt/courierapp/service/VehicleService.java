package com.nt.courierapp.service;

import com.nt.courierapp.message.request.VehicleForm;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.Vehicle;
import com.nt.courierapp.model.VehicleDriver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface VehicleService {
    Optional<Vehicle> createVehicle(VehicleForm vehicleForm);
    Optional<VehicleDriver> addDriverToVehicle(Vehicle vehicle, Driver driver);
    Page findBySearchFilter(String filter, Pageable pageable);
}
