package com.nt.courierapp.service;

import com.nt.courierapp.message.request.order.OrderForm;
import com.nt.courierapp.message.request.order.VendorOrderForm;
import com.nt.courierapp.message.response.order.OrderDistanceDto;
import com.nt.courierapp.message.response.order.DeliveryRequestDto;
import com.nt.courierapp.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Optional;

public interface OrderService {

    OrderDistanceDto calculateOrderDistanceAndAmount(String originLatLng, String destLatLng);
    Long calculateOrderAmountByDistanceInMetres(Long metres);
    DeliveryRequest createOrder(OrderForm orderForm);
    DeliveryRequest createVendorOrder(VendorOrderForm orderForm);
    Optional<DeliveryHistory> updateOrderHistory(DeliveryRequest deliveryRequest, DeliveryStatusConstant deliveryStatusConstant, PortalUser updatedBy);
    DeliveryRequestDto toDeliveryRequestDto(DeliveryRequest deliveryRequest);
    DeliveryRequest addDriverToDeliveryRequest(DeliveryRequest deliveryRequest, Driver driver);
    DeliveryRequest updateDeliveryAmount(DeliveryRequest deliveryRequest, BigDecimal amount);
    Page findBySearchFilter(String filter, Pageable pageable);
    Page findBySearchFilter(String filter, Pageable pageable, PortalUser portalUser);
    Page findByDriverAndSearchFilter(String filter, Pageable pageable, Driver driver);
}
