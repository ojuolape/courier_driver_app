package com.nt.courierapp.service;

import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.PortalUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Page findBySearchFilter(String userName, Pageable pageable);
    PortalUser createUser(SignUpForm signUpForm);
    Optional<PortalUser> getLoggedInUser();
    Optional<PortalUser> createDriverUser(Driver driver, String username);
    PortalUserDto toPortalUserDto(PortalUser portalUser);
    void createPasswordResetTokenForUser(PortalUser portalUser);
    void updateUserPassword(PortalUser portalUser, String password);

}
