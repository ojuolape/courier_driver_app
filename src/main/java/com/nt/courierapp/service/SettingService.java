package com.nt.courierapp.service;

import com.nt.courierapp.model.Settings;
import com.nt.courierapp.model.SettingsNameConstant;

import java.util.Optional;

public interface SettingService {
    String getSettingByName(SettingsNameConstant settingsNameConstant, String defaultValue);
    Optional<String> getSettingByName(SettingsNameConstant settingsNameConstant);
}
