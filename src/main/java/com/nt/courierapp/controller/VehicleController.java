package com.nt.courierapp.controller;

import com.nt.courierapp.message.request.VehicleForm;
import com.nt.courierapp.message.response.ResponseMessage;
import com.nt.courierapp.message.response.VehicleDto;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.Vehicle;
import com.nt.courierapp.repository.DriverRepository;
import com.nt.courierapp.repository.UserRepository;
import com.nt.courierapp.repository.VehicleDriverRepository;
import com.nt.courierapp.service.DriverService;
import com.nt.courierapp.service.UserService;
import com.nt.courierapp.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("vehicles")
public class VehicleController {

    @Autowired
    VehicleService vehicleService;
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    VehicleDriverRepository vehicleDriverRepository;
    @Autowired
    DriverService driverService;
    @Autowired
    DriverRepository driverRepository;

    @GetMapping
    public ResponseEntity searchDrivers(@RequestParam("q") Optional<String> searchFilter, @RequestParam("pageNumber") int pageNumber, @RequestParam("limit") int limit){
        Page result = vehicleService.findBySearchFilter(searchFilter.orElse(null), new PageRequest(pageNumber, limit));
        List<Vehicle> vehicles = result.getContent();
        Map<String, Object> response = new HashMap<>();
        response.put("results", vehicles.stream().map(it -> {
            VehicleDto dto = new VehicleDto();
            dto.setVehicleNumber(it.getVehicleNumber());
            dto.setCreatedBy(userService.toPortalUserDto(userRepository.getOne(it.getCreatedBy().getId())));
            List<Driver> drivers = vehicleDriverRepository.getVehicleActiveDriver(it.getId());
            if(!drivers.isEmpty()) {
                dto.setDriver(driverService.toDriverDto(driverRepository.getOne(drivers.get(0).getId())));
            }
            dto.setDateCreated(it.getDateCreated());
            return dto;
        }));
        response.put("pageNumber", pageNumber);
        response.put("limit", limit);
        response.put("total", result.getTotalElements());
        return ResponseEntity.ok(response);
    }
    @PostMapping
    public ResponseEntity<?> createVehicle(@Valid @RequestBody VehicleForm vehicleDto) {
        try {
            Vehicle vehicle = vehicleService.createVehicle(vehicleDto).orElseThrow(() -> new IllegalArgumentException("An error occurred, could not process request."));
            return new ResponseEntity<>(new ResponseMessage("Driver registered successfully!"), HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }


}
