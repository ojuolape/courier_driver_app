package com.nt.courierapp.controller;

import com.nt.courierapp.message.request.ChangePasswordDto;
import com.nt.courierapp.message.request.ResetPasswordForm;
import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.message.response.ResponseMessage;
import com.nt.courierapp.message.response.UserDto;
import com.nt.courierapp.model.PasswordResetToken;
import com.nt.courierapp.model.PortalUser;
import com.nt.courierapp.model.Role;
import com.nt.courierapp.model.RoleNameConstant;
import com.nt.courierapp.repository.PasswordResetRepository;
import com.nt.courierapp.repository.UserRepository;
import com.nt.courierapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordResetRepository passwordResetRepository;

    @GetMapping("/users")
    public ResponseEntity searchUsers(@RequestParam("q") Optional<String> searchFilter, @RequestParam("offset") int offset, @RequestParam("limit") int limit){
        Page result = userService.findBySearchFilter(searchFilter.orElse(null), new PageRequest(offset, limit));
        List<PortalUser> users = result.getContent();
        Map<String, Object> response = new HashMap<>();
        response.put("results", users.stream().map(it -> {
            UserDto userDto = new UserDto();
            userDto.setDateCreated(it.getDateCreated());
            userDto.setName(it.getName());
            userDto.setPhoneNumber(it.getPhoneNumber());
            userDto.setRoles(it.getRoles().stream().map(role-> role.getName().name()).collect(Collectors.toList()));
            userDto.setUsername(it.getUsername());
            return userDto;
        }));
        response.put("offset", offset);
        response.put("limit", limit);
        response.put("total", result.getTotalElements());
        return ResponseEntity.ok(response);
    }
    @PostMapping("/users/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Email is already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<>(new ResponseMessage("Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating portalUser's account
       PortalUser user = userService.createUser(signUpRequest);
        return new ResponseEntity<>(new ResponseMessage("PortalUser registered successfully!"), HttpStatus.OK);
    }


    @PostMapping("/user/resetPassword")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody ResetPasswordForm resetPasswordForm) {
        Optional<PortalUser> user = userRepository.findByEmail(resetPasswordForm.getEmail());
        if(!user.isPresent()){
            return ResponseEntity.badRequest().body("User does not exist");
        }

        userService.createPasswordResetTokenForUser(user.get());
        return new ResponseEntity<>(new ResponseMessage("Reset link sent successfully!"), HttpStatus.OK);
    }

    @GetMapping("/user/changePassword")
    public ResponseEntity<?> validateToken(@RequestParam("token") String token) {
        Optional<PasswordResetToken> passwordResetToken = passwordResetRepository.findByToken(token);

        if(!passwordResetToken.isPresent()){
            return ResponseEntity.badRequest().body("Invalid token");
        }

        if(passwordResetToken.get().getExpiryDate().isAfter(LocalDateTime.now())){
            return new ResponseEntity<>(new ResponseMessage("expired"), HttpStatus.OK);
        }
        PortalUser user = passwordResetToken.get().getPortalUser();
        /*Authentication auth = new UsernamePasswordAuthenticationToken(
                user, null, Arrays.asList(
                new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext().setAuthentication(auth);*/
        return new ResponseEntity<>(new ResponseMessage("valid"), HttpStatus.OK);
    }

    @PostMapping("/user/savePassword")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) {
        Optional<PasswordResetToken> passwordResetToken = passwordResetRepository.findByToken(changePasswordDto.getToken());

        if(!passwordResetToken.isPresent()){
            return ResponseEntity.badRequest().body("Invalid token");
        }
        if(passwordResetToken.get().getExpiryDate().isAfter(LocalDateTime.now())){
            return ResponseEntity.badRequest().body("expired");
        }

        userService.updateUserPassword(passwordResetToken.get().getPortalUser(), changePasswordDto.getPassword());
        return new ResponseEntity<>(new ResponseMessage("success"), HttpStatus.OK);
    }
}
