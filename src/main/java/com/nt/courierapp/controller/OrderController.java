package com.nt.courierapp.controller;

import com.nt.courierapp.message.request.order.OrderForm;
import com.nt.courierapp.message.request.order.UpdateOrderStatus;
import com.nt.courierapp.message.request.order.VendorOrderForm;
import com.nt.courierapp.message.response.QueryResult;
import com.nt.courierapp.message.response.ResponseMessage;
import com.nt.courierapp.message.response.VehicleDto;
import com.nt.courierapp.message.response.order.OrderDistanceDto;
import com.nt.courierapp.message.response.order.OrderResponse;
import com.nt.courierapp.model.DeliveryRequest;
import com.nt.courierapp.model.DeliveryStatusConstant;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.Vehicle;
import com.nt.courierapp.repository.DeliveryRequestRepository;
import com.nt.courierapp.repository.DriverRepository;
import com.nt.courierapp.service.OrderService;
import com.nt.courierapp.service.UserService;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/orders")
public class OrderController {

    @Autowired
    OrderService orderService;
    @Autowired
    DeliveryRequestRepository deliveryRequestRepository;
    @Autowired
    DriverRepository driverRepository;
    @Autowired
    UserService userService;

    @GetMapping
    @PreAuthorize("hasRole('VENDOR') or hasRole('ADMIN') or hasRole('DRIVER')")
    public ResponseEntity fetchOrders(HttpServletRequest request, @RequestParam("q") Optional<String> searchFilter, @RequestParam("pageNumber") int pageNumber, @RequestParam("limit") int limit) {
        Page result = null;
        if(request.isUserInRole("VENDOR")){
            result = orderService.findBySearchFilter(searchFilter.orElse(null), PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.desc("dateCreated"))), userService.getLoggedInUser().get());
        }else if(request.isUserInRole("DRIVER")){
            Optional<Driver> driver = driverRepository.findByPortalUser(userService.getLoggedInUser().get());
            if(!driver.isPresent()){
                return ResponseEntity.badRequest().body("driver not logged in");
            }
            result = orderService.findByDriverAndSearchFilter(searchFilter.orElse(null), PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.desc("dateCreated"))), driver.get());
        }
        else{
            result = orderService.findBySearchFilter(searchFilter.orElse(null), PageRequest.of(pageNumber, limit, Sort.by(Sort.Order.desc("dateCreated"))));
        }
        List<DeliveryRequest> deliveryRequests = result.getContent();
        Map<String, Object> response = new HashMap<>();
        response.put("results", deliveryRequests.stream().map(it -> orderService.toDeliveryRequestDto(it)));
        response.put("pageNumber", pageNumber);
        response.put("limit", limit);
        response.put("total", result.getTotalElements());
        return ResponseEntity.ok(response);
    }

    @GetMapping("estimate")
    public ResponseEntity<OrderDistanceDto> getOrderEstimate(@RequestParam("origin") String origin, @RequestParam("destination") String destination) {
            return ResponseEntity.ok(orderService.calculateOrderDistanceAndAmount(origin, destination));
    }

    @PostMapping({"", "extranet"})
    public ResponseEntity<?> createOrder(@Valid @RequestBody OrderForm orderForm) {
        try {
            DeliveryRequest deliveryRequest = orderService.createOrder(orderForm);
            return new ResponseEntity<>(new OrderResponse(deliveryRequest.getTrackingId()), HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("vendor")
    @PreAuthorize("hasRole('VENDOR')")
    public ResponseEntity<?> createVendorOrder(@Valid @RequestBody VendorOrderForm orderForm) {
        try {
            DeliveryRequest deliveryRequest = orderService.createVendorOrder(orderForm);
            return new ResponseEntity<>(new OrderResponse(deliveryRequest.getTrackingId()), HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("{trackingId}")
    public ResponseEntity fetchDeliveryRequest(@PathVariable("trackingId") String trackingId) {
        Optional<DeliveryRequest> deliveryRequest = deliveryRequestRepository.findByTrackingId(trackingId.toUpperCase());
        if(!deliveryRequest.isPresent()){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Delivery with Tracking Id - %s does not exist", trackingId)));
        }

        return ResponseEntity.ok(orderService.toDeliveryRequestDto(deliveryRequest.get()));
    }

    @PostMapping("{trackingId}/drivers/{driverId}")
    public ResponseEntity assignDriverToDeliveryRequest(@PathVariable("trackingId") String trackingId, @PathVariable("driverId") Long driverId) {
        Optional<DeliveryRequest> deliveryRequest = deliveryRequestRepository.findByTrackingId(trackingId.toUpperCase());
        if(!deliveryRequest.isPresent()){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Delivery with Tracking Id - %s does not exist", trackingId)));
        }
        if(deliveryRequest.get().getAmount() == null || deliveryRequest.get().getAmount() == 0L){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Update delivery amount before assigning a driver")));
        }
        Optional<Driver> driver = driverRepository.findById(driverId);
        if(!driver.isPresent()){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Driver does not exist")));
        }

        orderService.addDriverToDeliveryRequest(deliveryRequest.get(), driver.get());

        return ResponseEntity.ok(new ResponseMessage("Successful"));
    }

    @PostMapping("{trackingId}")
    public ResponseEntity updateDeliveryRequestAmount(@PathVariable("trackingId") String trackingId, @RequestParam("amount")BigDecimal amount) {
        Optional<DeliveryRequest> deliveryRequest = deliveryRequestRepository.findByTrackingId(trackingId.toUpperCase());
        if(!deliveryRequest.isPresent()){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Delivery with Tracking Id - %s does not exist", trackingId)));
        }

        orderService.updateDeliveryAmount(deliveryRequest.get(), amount);
        return ResponseEntity.ok(orderService.toDeliveryRequestDto(deliveryRequest.get()));
    }


    @PostMapping("{trackingId}/status")
    @PreAuthorize("hasRole('ADMIN') or hasRole('DRIVER')")
    public ResponseEntity updateDeliveryStatus(@PathVariable("trackingId") String trackingId, @Valid @RequestBody UpdateOrderStatus updateOrderStatus) {
        Optional<DeliveryRequest> deliveryRequest = deliveryRequestRepository.findByTrackingId(trackingId.toUpperCase());
        if(!deliveryRequest.isPresent()){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Delivery with Tracking Id - %s does not exist", trackingId)));
        }
        if(updateOrderStatus.getDeliveryStatus() == DeliveryStatusConstant.DELIVERED && (deliveryRequest.get().getAmount() == null || deliveryRequest.get().getAmount() == 0L)){
            return ResponseEntity.badRequest().body(new ResponseMessage(String.format("Update delivery amount before updating delivery status")));
        }
        orderService.updateOrderHistory(deliveryRequest.get(), updateOrderStatus.getDeliveryStatus(), userService.getLoggedInUser().get());
        return ResponseEntity.ok(orderService.toDeliveryRequestDto(deliveryRequest.get()));
    }
}
