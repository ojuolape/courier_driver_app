package com.nt.courierapp.controller;

import com.nt.courierapp.message.request.DriverVehicleForm;
import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.message.response.DriverVehicleDto;
import com.nt.courierapp.message.response.ResponseMessage;
import com.nt.courierapp.message.response.UserDto;
import com.nt.courierapp.message.response.VehicleDto;
import com.nt.courierapp.model.*;
import com.nt.courierapp.repository.DeliveryRequestRepository;
import com.nt.courierapp.repository.VehicleDriverRepository;
import com.nt.courierapp.repository.VehicleRepository;
import com.nt.courierapp.service.DriverService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("drivers")
public class DriverController {

    @Autowired
    DriverService driverService;

    @Autowired
    VehicleDriverRepository vehicleDriverRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    DeliveryRequestRepository deliveryRequestRepository;

    @GetMapping
    public ResponseEntity searchDrivers(@RequestParam("q") Optional<String> searchFilter, @RequestParam("pageNumber") int pageNumber, @RequestParam("limit") int limit){
        Page result = driverService.findBySearchFilter(searchFilter.orElse(null), new PageRequest(pageNumber, limit));
        List<Driver> users = result.getContent();
        Map<String, Object> response = new HashMap<>();
        response.put("results", users.stream().map(it -> {
            DriverVehicleDto dto = new DriverVehicleDto();
            dto.setDateCreated(it.getDateCreated());
            dto.setName(it.getName());
            dto.setId(it.getId());
            dto.setPhoneNumber(it.getPhoneNumber());
            dto.setEmailAddress(it.getEmailAddress());
            List<VehicleDriver> vehicleDrivers = vehicleDriverRepository.findAllByStatusAndDriver(GenericStatusConstant.ACTIVE, it);
            DeliveryStatusConstant[] statusConstants = {DeliveryStatusConstant.DELIVERED, DeliveryStatusConstant.CANCELLED};
            dto.setPendingDeliveries(deliveryRequestRepository.countAllByDriverAndOrderStatusNotIn(it,statusConstants));
            dto.setVehicles(vehicleDrivers.stream().map(vehicleDriver -> {
                Vehicle vehicle = vehicleRepository.getOne(vehicleDriver.getVehicle().getId());
                return new VehicleDto(vehicle.getVehicleNumber());
            }).collect(Collectors.toList()));
            dto.setVehiclesString(StringUtils.join(dto.getVehicles().stream().map(vehicleDto->vehicleDto.getVehicleNumber()).collect(Collectors.toList()), ", "));

            return dto;
        }));
        response.put("pageNumber", pageNumber);
        response.put("limit", limit);
        response.put("total", result.getTotalElements());
        return ResponseEntity.ok(response);
    }
    @PostMapping
    public ResponseEntity<?> createDriver(@Valid @RequestBody DriverVehicleForm driverVehicleForm) {
        try {
            Driver driver = driverService.createDriver(driverVehicleForm);
            return new ResponseEntity<>(new ResponseMessage("Driver registered successfully!"), HttpStatus.OK);
        }catch (IllegalArgumentException ex){
            return new ResponseEntity<>(new ResponseMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }


}
