package com.nt.courierapp.controller;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nt.courierapp.message.request.ChangePasswordDto;
import com.nt.courierapp.message.request.LoginForm;
import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.message.response.JwtResponse;
import com.nt.courierapp.message.response.ResponseMessage;
import com.nt.courierapp.model.PasswordResetToken;
import com.nt.courierapp.model.PortalUser;
import com.nt.courierapp.model.Role;
import com.nt.courierapp.model.RoleNameConstant;
import com.nt.courierapp.repository.PasswordResetRepository;
import com.nt.courierapp.repository.RoleRepository;
import com.nt.courierapp.repository.UserRepository;
import com.nt.courierapp.security.jwt.JwtProvider;
import com.nt.courierapp.security.services.UserPrinciple;
import com.nt.courierapp.service.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

	@Autowired
    AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	JwtProvider jwtProvider;



	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserPrinciple userInfo = (UserPrinciple) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userInfo.getUsername(), userInfo.getName(), userInfo.getAuthorities()));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		PortalUser portalUser = userService.createUser(signUpRequest);
		return new ResponseEntity<>(new ResponseMessage("PortalUser registered successfully!"), HttpStatus.OK);
	}

	@PostMapping("/driver/signin")
	public ResponseEntity<?> authenticateDriverUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserPrinciple userInfo = (UserPrinciple) authentication.getPrincipal();

		return ResponseEntity.ok(new JwtResponse(jwt, userInfo.getUsername(), userInfo.getName(), userInfo.getAuthorities().stream().map(it -> it.getAuthority()).collect(Collectors.toList())));
	}

}