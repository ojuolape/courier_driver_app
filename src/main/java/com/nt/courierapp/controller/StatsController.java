package com.nt.courierapp.controller;

import com.nt.courierapp.message.response.order.OrderStatsDto;
import com.nt.courierapp.model.DeliveryStatusConstant;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.PortalUser;
import com.nt.courierapp.repository.DeliveryRequestRepository;
import com.nt.courierapp.repository.DriverRepository;
import com.nt.courierapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("stats")
public class StatsController {

    @Autowired
    DeliveryRequestRepository deliveryRequestRepository;

    @Autowired
    UserService userService;

    @Autowired
    private DriverRepository driverRepository;


    @GetMapping("admin/orders")
    public ResponseEntity fetchAdminDashboardStats(HttpServletRequest request){
        LocalDate today = LocalDate.now();
        Map<String, Object> response = new HashMap<>();
        if(request.isUserInRole("ADMIN")) {
            response.put("countToday", deliveryRequestRepository.countAllByDateCreatedBetween(today.atStartOfDay(), today.plusDays(1).atStartOfDay()).orElse(0L));
            response.put("countThisMonth", deliveryRequestRepository.countAllByDateCreatedBetween(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay()).orElse(0L));
            response.put("countThisYear", deliveryRequestRepository.countAllByDateCreatedBetween(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay()).orElse(0L));
            response.put("totalToday", deliveryRequestRepository.totalCompletedOrders(today.atStartOfDay(), today.plusDays(1).atStartOfDay()).orElse(0L));
            response.put("totalThisMonth", deliveryRequestRepository.totalCompletedOrders(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay()).orElse(0L));
            response.put("totalThisYear", deliveryRequestRepository.totalCompletedOrders(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay()).orElse(0L));
            response.put("totalPendingDeliveries", deliveryRequestRepository.countAllByOrderStatus(DeliveryStatusConstant.PENDING).orElse(0L));
        } else if(request.isUserInRole("DRIVER")) {
            PortalUser portalUser = userService.getLoggedInUser().get();
            Driver driver = driverRepository.findByPortalUser(portalUser).get();
            response.put("countToday", deliveryRequestRepository.countAllByDateCreatedBetweenAndDriver(today.atStartOfDay(), today.plusDays(1).atStartOfDay(), driver).orElse(0L));
            response.put("countThisMonth", deliveryRequestRepository.countAllByDateCreatedBetweenAndDriver(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), driver).orElse(0L));
            response.put("countThisYear", deliveryRequestRepository.countAllByDateCreatedBetweenAndDriver(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), driver).orElse(0L));
            response.put("totalToday", deliveryRequestRepository.totalCompletedOrdersByDriver(today.atStartOfDay(), today.plusDays(1).atStartOfDay(), driver).orElse(0L));
            response.put("totalThisMonth", deliveryRequestRepository.totalCompletedOrdersByDriver(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), driver).orElse(0L));
            response.put("totalThisYear", deliveryRequestRepository.totalCompletedOrdersByDriver(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), driver).orElse(0L));
            response.put("totalPendingDeliveries", deliveryRequestRepository.countAllByOrderStatusAndDriver(DeliveryStatusConstant.PENDING, driver).orElse(0L));
        }else {
            PortalUser portalUser = userService.getLoggedInUser().get();
            response.put("countToday", deliveryRequestRepository.countAllByDateCreatedBetweenAndCreatedBy(today.atStartOfDay(), today.plusDays(1).atStartOfDay(), portalUser).orElse(0L));
            response.put("countThisMonth", deliveryRequestRepository.countAllByDateCreatedBetweenAndCreatedBy(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), portalUser).orElse(0L));
            response.put("countThisYear", deliveryRequestRepository.countAllByDateCreatedBetweenAndCreatedBy(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), portalUser).orElse(0L));
            response.put("totalToday", deliveryRequestRepository.totalCompletedOrdersByCreatedBy(today.atStartOfDay(), today.plusDays(1).atStartOfDay(), portalUser).orElse(0L));
            response.put("totalThisMonth", deliveryRequestRepository.totalCompletedOrdersByCreatedBy(today.withDayOfMonth(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), portalUser).orElse(0L));
            response.put("totalThisYear", deliveryRequestRepository.totalCompletedOrdersByCreatedBy(today.withDayOfYear(1).atStartOfDay(), today.with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(), portalUser).orElse(0L));
            response.put("totalPendingDeliveries", deliveryRequestRepository.countAllByOrderStatusAndCreatedBy(DeliveryStatusConstant.PENDING, portalUser).orElse(0L));
        }

        return ResponseEntity.ok().body(response);
    }


}
