package com.nt.courierapp.listener;

import com.nt.courierapp.service.InitializationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.IOException;

@Component
public class StartupApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger log = LoggerFactory.getLogger(StartupApplicationListener.class);

    @Autowired
    private InitializationService initializationService;

    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("App started");
        try {
            initializationService.loadRoles();
            initializationService.createAdminUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
