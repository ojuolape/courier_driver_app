package com.nt.courierapp.repository;

import com.nt.courierapp.model.Settings;
import com.nt.courierapp.model.SettingsNameConstant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Long> {
    Optional<Settings> getSettingsByName(SettingsNameConstant settingsNameConstant);
    @Query("select value from Settings where name = ?1")
    Optional<String> getSettingsValueByName(SettingsNameConstant settingsNameConstant);
}
