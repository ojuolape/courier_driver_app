package com.nt.courierapp.repository;

import com.nt.courierapp.model.PasswordResetToken;
import com.nt.courierapp.model.PortalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PasswordResetRepository extends JpaRepository<PasswordResetToken, Long> {
    Optional<PasswordResetToken> findByPortalUser(PortalUser portalUser);
    Optional<PasswordResetToken> findByToken(String token);
}
