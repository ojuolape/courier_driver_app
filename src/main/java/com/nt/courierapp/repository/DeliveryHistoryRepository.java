package com.nt.courierapp.repository;

import com.nt.courierapp.model.DeliveryRequest;
import com.nt.courierapp.model.DeliveryStatusConstant;
import com.nt.courierapp.model.DeliveryHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeliveryHistoryRepository extends JpaRepository<DeliveryHistory, Long> {
    Optional<DeliveryHistory> findByDeliveryRequestAndStatus(DeliveryRequest deliveryRequest, DeliveryStatusConstant deliveryStatusConstant);
    List<DeliveryHistory> findAllByDeliveryRequest(DeliveryRequest deliveryRequest);
}
