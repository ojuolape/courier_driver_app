package com.nt.courierapp.repository;

import com.nt.courierapp.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface DeliveryRequestRepository extends JpaRepository<DeliveryRequest, Long>, JpaSpecificationExecutor {
    Optional<DeliveryRequest> findByTrackingId(String trackingId);
    Long countAllByDriverAndOrderStatusIn(Driver driver, DeliveryStatusConstant[] status);
    Long countAllByDriverAndOrderStatusNotIn(Driver driver, DeliveryStatusConstant[] status);
    List<DeliveryRequest> findAll();
    Optional<Long> countAllByDateCreatedBetween(LocalDateTime startTime, LocalDateTime endTime);
    Optional<Long> countAllByOrderStatus(DeliveryStatusConstant statusConstant);
    @Query("select sum(amount) from DeliveryRequest where orderStatus = 'DELIVERED' and dateCreated between ?1 and ?2 ")
    Optional<Long> totalCompletedOrders(LocalDateTime startTime, LocalDateTime endTime);
    Optional<Long> countAllByDateCreatedBetweenAndOrderStatus(LocalDateTime startTime, LocalDateTime endTime, GenericStatusConstant genericStatusConstant);
    Optional<Long> countAllByDateCreatedBetweenAndCreatedBy(LocalDateTime startTime, LocalDateTime endTime, PortalUser portalUser);
    Optional<Long> countAllByOrderStatusAndCreatedBy(DeliveryStatusConstant statusConstant, PortalUser portalUser);
    @Query("select sum(amount) from DeliveryRequest where orderStatus = 'DELIVERED' and dateCreated between ?1 and ?2 and createdBy = ?3")
    Optional<Long> totalCompletedOrdersByCreatedBy(LocalDateTime startTime, LocalDateTime endTime, PortalUser portalUser);
    Optional<Long> countAllByDateCreatedBetweenAndDriver(LocalDateTime startTime, LocalDateTime endTime, Driver driver);
    @Query("select sum(amount) from DeliveryRequest where orderStatus = 'DELIVERED' and dateCreated between ?1 and ?2 and driver = ?3")
    Optional<Long> totalCompletedOrdersByDriver(LocalDateTime startTime, LocalDateTime endTime, Driver driver);
    Optional<Long> countAllByOrderStatusAndDriver(DeliveryStatusConstant statusConstant, Driver driver);

}
