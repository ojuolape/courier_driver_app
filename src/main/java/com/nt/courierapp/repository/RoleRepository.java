package com.nt.courierapp.repository;

import com.nt.courierapp.model.Role;
import com.nt.courierapp.model.RoleNameConstant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleNameConstant roleName);
}