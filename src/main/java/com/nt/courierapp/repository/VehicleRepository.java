package com.nt.courierapp.repository;

import com.nt.courierapp.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long>,JpaSpecificationExecutor<Vehicle> {
    Optional<Vehicle> findByVehicleNumber(String vehicleNumber);
    List<Vehicle> findAll();
}
