package com.nt.courierapp.repository;

import com.nt.courierapp.model.PortalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<PortalUser, Long> , JpaSpecificationExecutor<PortalUser>{
    Optional<PortalUser> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    List<PortalUser> findAll();
    Optional<PortalUser> findByEmail(String email);
}