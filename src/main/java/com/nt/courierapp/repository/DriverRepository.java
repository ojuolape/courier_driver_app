package com.nt.courierapp.repository;

import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.PortalUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long>, JpaSpecificationExecutor<Driver> {
    List<Driver> findAll();
    Optional<Driver> findByEmailAddress(String email);
    Optional<Driver> findByPhoneNumber(String phoneNumber);
    Optional<Driver> findByPortalUser(PortalUser portalUser);
}
