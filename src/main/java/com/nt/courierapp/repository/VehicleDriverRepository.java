package com.nt.courierapp.repository;

import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.GenericStatusConstant;
import com.nt.courierapp.model.Vehicle;
import com.nt.courierapp.model.VehicleDriver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VehicleDriverRepository extends JpaRepository<VehicleDriver, Long> {
    List<VehicleDriver> findAllByStatusAndVehicle(GenericStatusConstant statusConstant, Vehicle vehicle);
    List<VehicleDriver> findAllByStatusAndDriver(GenericStatusConstant statusConstant, Driver vehicle);
    @Modifying
    @Query("update VehicleDriver set status = 'INACTIVE' where vehicle.id = ?1")
    void inactivateAllVehicleDriverByVehicle(Long vehicleId);
    @Query("select v.driver from VehicleDriver v where v.status = 'ACTIVE' and v.vehicle.id = ?1")
    List<Driver> getVehicleActiveDriver(Long vehicleId);
    @Query("select v.vehicle.vehicleNumber from VehicleDriver  v where v.status = 'ACTIVE' and v.driver.id = ?1")
    List<String> findAllVehiclesByDriver(Long driverId);

}
