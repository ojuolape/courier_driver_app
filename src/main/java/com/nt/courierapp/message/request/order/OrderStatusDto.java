package com.nt.courierapp.message.request.order;

import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.DeliveryStatusConstant;

import java.time.LocalDateTime;

public class OrderStatusDto {
    private DeliveryStatusConstant orderStatus ;
    private LocalDateTime dateCreated;
    private PortalUserDto createdBy;

    public DeliveryStatusConstant getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(DeliveryStatusConstant orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalUserDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(PortalUserDto createdBy) {
        this.createdBy = createdBy;
    }
}
