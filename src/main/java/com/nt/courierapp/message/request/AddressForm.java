package com.nt.courierapp.message.request;

import com.nt.courierapp.model.Address;

import javax.validation.constraints.NotBlank;

public class AddressForm {

    private String longitude;
    private String latitude;
    @NotBlank
    private String streetAddress;

    public AddressForm() {
    }

    public AddressForm(Address pickUpAddress) {
        this.latitude = pickUpAddress.getLatitude();
        this.longitude = pickUpAddress.getLongitude();
        this.streetAddress = pickUpAddress.getStreetAddress();
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
}
