package com.nt.courierapp.message.request.order;

import com.nt.courierapp.model.DeliveryStatusConstant;

import javax.validation.constraints.NotNull;

public class UpdateOrderStatus {

    @NotNull
    private DeliveryStatusConstant deliveryStatus;

    public DeliveryStatusConstant getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatusConstant deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }
}
