package com.nt.courierapp.message.request;

import javax.validation.constraints.NotBlank;

public class DriverVehicleForm {
    @NotBlank(message = "Driver's name is a required field")
    private String name;
    @NotBlank(message = "Driver's phone number is a required field")
    private String phoneNumber;
    private String emailAddress;
    @NotBlank(message = "Username is a required field")
    private String username;
    @NotBlank(message = "Vehicle Number is a required field")
    private String vehicleNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
