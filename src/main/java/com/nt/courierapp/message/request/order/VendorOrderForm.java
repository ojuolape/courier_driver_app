package com.nt.courierapp.message.request.order;

import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.message.response.DriverDto;
import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.DeliveryTypeConstant;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class VendorOrderForm {
    @NotBlank
    private String recipientName;
    @NotBlank
    private String recipientPhoneNo;
    private String recipientEmail;
    private String deliveryNote;
    private Long estimatedAmount;
    private Long amount;
    @NotNull
    @Valid
    private AddressForm deliveryAddress;
    @Valid
    @NotNull
    private AddressForm pickUpAddress;
    private String deliveryItem;
    private DeliveryTypeConstant deliveryType;

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientPhoneNo() {
        return recipientPhoneNo;
    }

    public void setRecipientPhoneNo(String recipientPhoneNo) {
        this.recipientPhoneNo = recipientPhoneNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public Long getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Long estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public AddressForm getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressForm deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AddressForm getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(AddressForm pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveryItem() {
        return deliveryItem;
    }

    public void setDeliveryItem(String deliveryItem) {
        this.deliveryItem = deliveryItem;
    }

    public DeliveryTypeConstant getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryTypeConstant deliveryType) {
        this.deliveryType = deliveryType;
    }
}
