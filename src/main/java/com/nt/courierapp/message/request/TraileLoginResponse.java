package com.nt.courierapp.message.request;

public class TraileLoginResponse {
    private int status;
    private String message;
    private String user_api_hash;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_api_hash() {
        return user_api_hash;
    }

    public void setUser_api_hash(String user_api_hash) {
        this.user_api_hash = user_api_hash;
    }
}
