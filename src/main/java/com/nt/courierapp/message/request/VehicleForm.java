package com.nt.courierapp.message.request;

import com.nt.courierapp.model.VehicleTypeConstant;

import javax.validation.constraints.NotBlank;

public class VehicleForm {
    @NotBlank
    private String vehicleNumber;
    private VehicleTypeConstant vehicleType = VehicleTypeConstant.MOTORCYCLE;

    public VehicleForm() {
    }

    public VehicleForm(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public VehicleTypeConstant getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleTypeConstant vehicleType) {
        this.vehicleType = vehicleType;
    }
}
