package com.nt.courierapp.message.request.order;

import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.message.response.DriverDto;
import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.DeliveryTypeConstant;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class OrderForm {
    @NotBlank
    private String customerName;
    @NotBlank
    private String customerPhoneNo;
    private String customerEmail;
    @NotBlank
    private String recipientName;
    @NotBlank
    private String recipientPhoneNo;
    private String recipientEmail;
    private String deliveryNote;
    private Long estimatedAmount;
    private Long amount;
    @NotNull
    @Valid
    private AddressForm deliveryAddress;
    @Valid
    @NotNull
    private AddressForm pickUpAddress;
    private String deliveryItem;
    private String orderStatus;
    private LocalDateTime dateCreated;
    private PortalUserDto createdBy;
    private DriverDto driver;
    private PortalUserDto lastUpdatedBy;
    private List<OrderStatusDto> orderStatusInfo;
    private DeliveryTypeConstant deliveryType;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientPhoneNo() {
        return recipientPhoneNo;
    }

    public void setRecipientPhoneNo(String recipientPhoneNo) {
        this.recipientPhoneNo = recipientPhoneNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public Long getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Long estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public AddressForm getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressForm deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AddressForm getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(AddressForm pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveryItem() {
        return deliveryItem;
    }

    public void setDeliveryItem(String deliveryItem) {
        this.deliveryItem = deliveryItem;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalUserDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(PortalUserDto createdBy) {
        this.createdBy = createdBy;
    }

    public DriverDto getDriver() {
        return driver;
    }

    public void setDriver(DriverDto driver) {
        this.driver = driver;
    }

    public PortalUserDto getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(PortalUserDto lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public List<OrderStatusDto> getOrderStatusInfo() {
        return orderStatusInfo;
    }

    public void setOrderStatusInfo(List<OrderStatusDto> orderStatusInfo) {
        this.orderStatusInfo = orderStatusInfo;
    }

    public DeliveryTypeConstant getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryTypeConstant deliveryType) {
        this.deliveryType = deliveryType;
    }
}
