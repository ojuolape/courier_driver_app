package com.nt.courierapp.message.dto.google_gps;

public class Element {
    private Distance distance;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }
}
