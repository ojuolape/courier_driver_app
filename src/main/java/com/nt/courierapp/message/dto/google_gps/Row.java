package com.nt.courierapp.message.dto.google_gps;

public class Row {
    private Element[] elements;

    public Element[] getElements() {
        return elements;
    }

    public void setElements(Element[] elements) {
        this.elements = elements;
    }
}
