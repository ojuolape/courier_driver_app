package com.nt.courierapp.message.dto.google_gps;

public class GpsDistanceDto {
    private Row[] rows;

    public Row[] getRows() {
        return rows;
    }

    public void setRows(Row[] rows) {
        this.rows = rows;
    }
}
