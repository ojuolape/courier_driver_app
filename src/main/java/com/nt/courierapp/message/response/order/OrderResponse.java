package com.nt.courierapp.message.response.order;

public class OrderResponse {
    private String trackingId;

    public OrderResponse(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }
}
