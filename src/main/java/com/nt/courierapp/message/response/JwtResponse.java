package com.nt.courierapp.message.response;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String username;
	private String displayName;
	private List<String> roles;
	private Collection<? extends GrantedAuthority> authorities;

	public JwtResponse(String accessToken, String username, String name, Collection<? extends GrantedAuthority> authorities) {
		this.token = accessToken;
		this.username = username;
		this.authorities = authorities;
		this.displayName = name;
	}

	public JwtResponse(String accessToken, String username, String name, List<String> authorities) {
		this.token = accessToken;
		this.username = username;
		this.roles = authorities;
		this.displayName = name;
	}
	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}