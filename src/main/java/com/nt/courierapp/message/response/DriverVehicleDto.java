package com.nt.courierapp.message.response;

import java.time.LocalDateTime;
import java.util.List;

public class DriverVehicleDto {
    private Long id;
    private String name;
    private String phoneNumber;
    private String emailAddress;
    private LocalDateTime dateCreated;
    private PortalUserDto createdBy;
    private List<VehicleDto> vehicles;
    private Long pendingDeliveries;
    private String vehiclesString;

    public DriverVehicleDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalUserDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(PortalUserDto createdBy) {
        this.createdBy = createdBy;
    }

    public List<VehicleDto> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleDto> vehicles) {
        this.vehicles = vehicles;
    }

    public Long getPendingDeliveries() {
        return pendingDeliveries;
    }

    public void setPendingDeliveries(Long pendingDeliveries) {
        this.pendingDeliveries = pendingDeliveries;
    }

    public String getVehiclesString() {
        return vehiclesString;
    }

    public void setVehiclesString(String vehiclesString) {
        this.vehiclesString = vehiclesString;
    }
}
