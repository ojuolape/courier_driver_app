package com.nt.courierapp.message.response.order;

import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.message.request.order.OrderStatusDto;
import com.nt.courierapp.message.response.DriverDto;
import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.DeliveryHistory;
import com.nt.courierapp.model.DeliveryRequest;
import com.nt.courierapp.model.DeliveryStatusConstant;

import java.time.LocalDateTime;
import java.util.List;

public class DeliveryRequestDto {
    private DeliveryStatusConstant status;
    private String customerName;
    private String customerPhoneNo;
    private String customerEmail;
    private String recipientPhoneNo;
    private String recipientEmail;
    private String recipientName;
    private String deliveryNote;
    private Long estimatedAmount;
    private Long amount;
    private AddressForm deliveryAddress;
    private AddressForm pickUpAddress;
    private String deliveryItem;
    private LocalDateTime dateCreated;
    private PortalUserDto createdBy;
    private DriverDto driver;
    private PortalUserDto lastUpdatedBy;
    private String trackingId;
    private List<DeliveryHistory> deliveryHistory;


    public DeliveryRequestDto(DeliveryRequest deliveryRequest) {
        this.customerName = deliveryRequest.getCustomerName();
        this.customerEmail = deliveryRequest.getCustomerEmail();
        this.amount = deliveryRequest.getAmount();
        this.customerPhoneNo = deliveryRequest.getCustomerPhoneNo();
        this.dateCreated = deliveryRequest.getDateCreated();
        this.deliveryItem = deliveryRequest.getDeliveryItem();
        this.estimatedAmount = deliveryRequest.getEstimatedAmount();
        this.status = deliveryRequest.getOrderStatus();
        this.recipientEmail = deliveryRequest.getRecipientEmail();
        this.recipientPhoneNo = deliveryRequest.getRecipientPhoneNo();
        this.recipientName = deliveryRequest.getRecipientName();
        this.trackingId = deliveryRequest.getTrackingId();
    }

    public DeliveryStatusConstant getStatus() {
        return status;
    }

    public void setStatus(DeliveryStatusConstant status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getRecipientPhoneNo() {
        return recipientPhoneNo;
    }

    public void setRecipientPhoneNo(String recipientPhoneNo) {
        this.recipientPhoneNo = recipientPhoneNo;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public Long getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Long estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public AddressForm getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressForm deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AddressForm getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(AddressForm pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveryItem() {
        return deliveryItem;
    }

    public void setDeliveryItem(String deliveryItem) {
        this.deliveryItem = deliveryItem;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalUserDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(PortalUserDto createdBy) {
        this.createdBy = createdBy;
    }

    public DriverDto getDriver() {
        return driver;
    }

    public void setDriver(DriverDto driver) {
        this.driver = driver;
    }

    public PortalUserDto getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(PortalUserDto lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public List<DeliveryHistory> getDeliveryHistory() {
        return deliveryHistory;
    }

    public void setDeliveryHistory(List<DeliveryHistory> deliveryHistory) {
        this.deliveryHistory = deliveryHistory;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }
}
