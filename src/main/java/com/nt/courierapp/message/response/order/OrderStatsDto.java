package com.nt.courierapp.message.response.order;

public class OrderStatsDto {
    int totalPendingDelivery;
    double amountMade;
    int totalCompletedDelivery;

    public OrderStatsDto(int totalPendingDelivery, double amountMade, int totalCompletedDelivery) {
        this.totalPendingDelivery = totalPendingDelivery;
        this.amountMade = amountMade;
        this.totalCompletedDelivery = totalCompletedDelivery;
    }

    public int getTotalPendingDelivery() {
        return totalPendingDelivery;
    }

    public void setTotalPendingDelivery(int totalPendingDelivery) {
        this.totalPendingDelivery = totalPendingDelivery;
    }

    public double getAmountMade() {
        return amountMade;
    }

    public void setAmountMade(double amountMade) {
        this.amountMade = amountMade;
    }

    public int getTotalCompletedDelivery() {
        return totalCompletedDelivery;
    }

    public void setTotalCompletedDelivery(int totalCompletedDelivery) {
        this.totalCompletedDelivery = totalCompletedDelivery;
    }
}
