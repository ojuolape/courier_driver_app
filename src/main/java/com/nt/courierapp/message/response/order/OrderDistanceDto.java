package com.nt.courierapp.message.response.order;

public class OrderDistanceDto {
    private String distance;
    private Long amount;

    public OrderDistanceDto(String distance, Long amount) {
        this.distance = distance;
        this.amount = amount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
