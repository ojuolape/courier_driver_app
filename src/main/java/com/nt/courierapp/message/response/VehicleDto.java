package com.nt.courierapp.message.response;


import java.time.LocalDateTime;

public class VehicleDto {
    private String vehicleNumber;
    private PortalUserDto createdBy;
    private DriverDto driver;
    private LocalDateTime dateCreated;

    public VehicleDto() {
    }

    public PortalUserDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(PortalUserDto createdBy) {
        this.createdBy = createdBy;
    }

    public DriverDto getDriver() {
        return driver;
    }

    public void setDriver(DriverDto driverDto) {
        this.driver = driverDto;
    }

    public VehicleDto(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }
}
