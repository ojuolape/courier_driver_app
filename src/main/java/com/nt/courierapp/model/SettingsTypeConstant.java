package com.nt.courierapp.model;

public enum SettingsTypeConstant {
    SYSTEM, PORTAL
}
