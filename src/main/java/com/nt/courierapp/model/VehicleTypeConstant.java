package com.nt.courierapp.model;

public enum VehicleTypeConstant {
    MOTORCYCLE, TRUCK
}
