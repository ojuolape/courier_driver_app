package com.nt.courierapp.model;

public enum GenericStatusConstant {
    ACTIVE, INACTIVE, DELETED
}
