package com.nt.courierapp.model;

public enum DeliveryStatusConstant {
    PENDING, DELIVERED, IN_TRANSIT, CANCELLED, DRIVER_ASSIGNED
}
