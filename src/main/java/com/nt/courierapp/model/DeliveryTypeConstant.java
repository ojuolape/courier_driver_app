package com.nt.courierapp.model;

public enum DeliveryTypeConstant {
    SAME_DAY, EXPRESS
}
