package com.nt.courierapp.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "settings")
public class Settings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private PortalUser lastUpdatedBy;
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private SettingsTypeConstant type;
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    private SettingsNameConstant name;
    @NotBlank
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PortalUser getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(PortalUser lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public SettingsTypeConstant getType() {
        return type;
    }

    public void setType(SettingsTypeConstant type) {
        this.type = type;
    }

    public SettingsNameConstant getName() {
        return name;
    }

    public void setName(SettingsNameConstant name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
