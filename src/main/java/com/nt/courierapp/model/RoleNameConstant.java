package com.nt.courierapp.model;

public enum  RoleNameConstant {
    ADMIN, PARTNER, VENDOR, USER, DRIVER
}