package com.nt.courierapp.model;

public enum SettingsNameConstant {
    DEFAULT_PRICE_PER_KILOMETER_IN_KOBO,
    EMAIL_PASSWORD,
    EMAIL_USERNAME,
    SMTP_HOST,
    SMTP_PORT,
    HOST_URL,
    NEW_DELIVERY_REQUEST_NOTIFICATION_RECIPIENT,
    TRAILE_API_BASE_URL,
    TRAILE_USERNAME,
    TRAILE_PASSWORD,
    TRAILE_USER_API_HASH,
}
