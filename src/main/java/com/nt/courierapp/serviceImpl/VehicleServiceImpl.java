package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.message.request.VehicleForm;
import com.nt.courierapp.model.Driver;
import com.nt.courierapp.model.GenericStatusConstant;
import com.nt.courierapp.model.Vehicle;
import com.nt.courierapp.model.VehicleDriver;
import com.nt.courierapp.repository.VehicleDriverRepository;
import com.nt.courierapp.repository.VehicleRepository;
import com.nt.courierapp.service.UserService;
import com.nt.courierapp.service.VehicleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleRepository vehicleRepository;
    @Autowired
    UserService userService;
    @Autowired
    VehicleDriverRepository vehicleDriverRepository;

    @Override
    @Transactional
    public Optional<Vehicle> createVehicle(VehicleForm vehicleForm) {
        Optional<Vehicle> existingVehicle = vehicleRepository.findByVehicleNumber(vehicleForm.getVehicleNumber());
        if(existingVehicle.isPresent()){
            throw new IllegalArgumentException("Vehicle number already exists");
        }

        Vehicle vehicle = new Vehicle();
        vehicle.setDateCreated(LocalDateTime.now());
        vehicle.setVehicleNumber(vehicleForm.getVehicleNumber());
        vehicle.setVehicleType(vehicleForm.getVehicleType());
        vehicle.setCreatedBy(userService.getLoggedInUser().orElseThrow(() -> new RuntimeException("Unauthorized")));
        vehicleRepository.save(vehicle);
        return Optional.ofNullable(vehicle);
    }

    @Override
    @Transactional
    public Optional<VehicleDriver> addDriverToVehicle(Vehicle vehicle, Driver driver) {
        vehicleDriverRepository.inactivateAllVehicleDriverByVehicle(vehicle.getId());
        VehicleDriver vehicleDriver = new VehicleDriver();
        vehicleDriver.setCreatedBy(userService.getLoggedInUser().orElseThrow(() -> new RuntimeException("Unauthorized")));
        vehicleDriver.setDriver(driver);
        vehicleDriver.setStatus(GenericStatusConstant.ACTIVE);
        vehicleDriver.setVehicle(vehicle);
        vehicleDriverRepository.save(vehicleDriver);
        return Optional.ofNullable(vehicleDriver);
    }

    @Override
    public Page findBySearchFilter(String filter, Pageable pageable) {
        Page page = vehicleRepository.findAll(new Specification<Vehicle>() {
            @Override
            public Predicate toPredicate(Root<Vehicle> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(StringUtils.isNotBlank(filter)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("vehicleNumber")), ""+filter.toLowerCase()+"%")));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }
}
