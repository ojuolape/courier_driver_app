package com.nt.courierapp.serviceImpl;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.nt.courierapp.message.request.LoginForm;
import com.nt.courierapp.message.request.TraileLoginResponse;
import com.nt.courierapp.model.SettingsNameConstant;
import com.nt.courierapp.service.SettingService;
import com.nt.courierapp.service.TraileApiClient;
import com.nt.courierapp.utils.ApiCallExceptionForOkHttp;
import com.nt.courierapp.utils.ApiClient;
import com.nt.courierapp.utils.ByteArrayDataSource;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class TraileApiClientImpl implements TraileApiClient {

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ApiClient apiClient;

    @Autowired
    private SettingService settingService;

    @Autowired
    private ObjectMapper objectMapper;


    private Gson gson = new Gson();
    private String apiBaseUrl = "";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");


    @PostConstruct
    public void init() {
        if (StringUtils.isBlank(apiBaseUrl)) {
            apiBaseUrl = settingService.getSettingByName(SettingsNameConstant.TRAILE_API_BASE_URL, "http://104.155.233.3/api");
        }
        logger.info("traile apiBaseUrl: {}", apiBaseUrl);
        apiClient = new ApiClient(new OkHttpClient()) {

            @Override
            protected String getBaseUrl() {
                return apiBaseUrl;
            }
        };
    }

    @Override
    public TraileLoginResponse login(LoginForm loginForm) {
        String er = gson.toJson(loginForm);
        try (Response response = apiClient.getResponse("POST", new Request.Builder()
                .url(apiBaseUrl + "/login"),
                toJsonDataSource(loginForm))) {
            if (response.isSuccessful()) {
                String json = null;
                try {
                    json = response.body().string();
                    TraileLoginResponse apiResponse = gson.fromJson(json, TraileLoginResponse.class);
                    return apiResponse;
                } catch (IOException e) {
                    if (json != null) {
                        logger.info("---------->response: ({})->{}", response.code(), json);
                    }
                    throw new RuntimeException(e);
                }
            }
            throw new ApiCallExceptionForOkHttp(response);
        }
    }

    public DataSource toJsonDataSource(Object apiResourcePortalUser) {
        try {
            byte[] data = objectMapper.writerFor(apiResourcePortalUser.getClass()).writeValueAsBytes(apiResourcePortalUser);
            return new ByteArrayDataSource(data,
                    "application/json");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}


