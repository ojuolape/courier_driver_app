package com.nt.courierapp.serviceImpl.sequence;

import com.nt.courierapp.service.qualifier.TrackingIdSequence;
import org.springframework.stereotype.Component;

@Component
@TrackingIdSequence
public class TrackingIdSequenceService extends SequenceServiceImpl {
    public TrackingIdSequenceService() {
        super("tracking_id");
    }
}
