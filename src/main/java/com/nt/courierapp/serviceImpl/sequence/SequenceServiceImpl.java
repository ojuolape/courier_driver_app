package com.nt.courierapp.serviceImpl.sequence;

import com.nt.courierapp.service.sequence.SequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.PostConstruct;

public class SequenceServiceImpl implements SequenceService {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final String sequenceName;

    public SequenceServiceImpl(String sequenceTableName) {
        this.sequenceName = sequenceTableName.toLowerCase() + "_sequence";
    }

    @PostConstruct
    public void init() {
        this.jdbcTemplate.execute(String.format("DO $$ BEGIN CREATE SEQUENCE %s; EXCEPTION WHEN duplicate_table THEN END $$ LANGUAGE plpgsql;", sequenceName));
    }

    @Override
    public Long getNextId() {
        return this.jdbcTemplate.queryForObject(String.format("select nextval ('%s')", sequenceName), Long.class);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
