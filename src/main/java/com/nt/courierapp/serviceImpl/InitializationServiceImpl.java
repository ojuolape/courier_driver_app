package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.model.Role;
import com.nt.courierapp.model.RoleNameConstant;
import com.nt.courierapp.repository.RoleRepository;
import com.nt.courierapp.repository.UserRepository;
import com.nt.courierapp.service.InitializationService;
import com.nt.courierapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InitializationServiceImpl implements InitializationService {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void createAdminUser() {
        SignUpForm signUpForm = new SignUpForm();
        signUpForm.setEmail("ojuolapedaina@gmail.com");
        signUpForm.setName("Admin");
        signUpForm.setPassword("pass123");
        signUpForm.setUsername("admin");
        Set<String> roles = new HashSet<>();
        roles.add("ADMIN");
        signUpForm.setRole(roles);

        if(!userRepository.existsByUsername(signUpForm.getUsername())){
            userService.createUser(signUpForm);
        }

    }

    @Override
    public void loadRoles() {
        if(!roleRepository.findByName(RoleNameConstant.ADMIN).isPresent() ) {
            Role role = new Role();
            role.setName(RoleNameConstant.ADMIN);
            roleRepository.save(role);
        }

        if(!roleRepository.findByName(RoleNameConstant.PARTNER).isPresent() ) {
            Role role = new Role();
            role.setName(RoleNameConstant.PARTNER);
            roleRepository.save(role);
        }

        if(!roleRepository.findByName(RoleNameConstant.DRIVER).isPresent() ) {
            Role role = new Role();
            role.setName(RoleNameConstant.DRIVER);
            roleRepository.save(role);
        }

        if(!roleRepository.findByName(RoleNameConstant.VENDOR).isPresent() ) {
            Role role = new Role();
            role.setName(RoleNameConstant.VENDOR);
            roleRepository.save(role);
        }
    }
}
