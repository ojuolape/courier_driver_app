package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.message.request.SignUpForm;
import com.nt.courierapp.message.response.PortalUserDto;
import com.nt.courierapp.model.*;
import com.nt.courierapp.repository.PasswordResetRepository;
import com.nt.courierapp.repository.RoleRepository;
import com.nt.courierapp.repository.UserRepository;
import com.nt.courierapp.service.MailService;
import com.nt.courierapp.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    MailService mailService;

    @Autowired
    PasswordResetRepository passwordResetRepository;


    @Override
    public Page findBySearchFilter(String userName, Pageable pageable){
        Page page = userRepository.findAll(new Specification<PortalUser>() {
            @Override
            public Predicate toPredicate(Root<PortalUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(StringUtils.isNotBlank(userName)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%"+userName.toLowerCase()+"%")));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }

    @Override
    public PortalUser createUser(SignUpForm signUpRequest) {
        PortalUser portalUser = new PortalUser(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()), signUpRequest.getPhoneNumber());

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            switch (role) {
                case "ADMIN":
                    Role adminRole = roleRepository.findByName(RoleNameConstant.ADMIN)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: PortalUser Role not find."));
                    roles.add(adminRole);

                    break;

                case "PARTNER":
                    Role pmRole = roleRepository.findByName(RoleNameConstant.PARTNER)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Partner Role not find."));
                    roles.add(pmRole);

                    break;
                case "VENDOR":
                    Role vendorRole = roleRepository.findByName(RoleNameConstant.VENDOR)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: Vendor Role not find."));
                    roles.add(vendorRole);

                    break;
                default:
                    Role userRole = roleRepository.findByName(RoleNameConstant.USER)
                            .orElseThrow(() -> new RuntimeException("Fail! -> Cause: PortalUser Role not find."));
                    roles.add(userRole);
            }
        });

        portalUser.setRoles(roles);
        userRepository.save(portalUser);
        mailService.sendNewUserMail(portalUser, signUpRequest.getPassword());
        return portalUser;
    }

    @Override
    public Optional<PortalUser> getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            return userRepository.findByUsername(currentUserName);
        }
        return Optional.empty();
    }

    @Override
    public Optional<PortalUser> createDriverUser(Driver driver, String username) {

        PortalUser portalUser = new PortalUser(driver.getName(), username, driver.getEmailAddress(),
                encoder.encode("p@ssw0rd"), driver.getPhoneNumber());

        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName(RoleNameConstant.DRIVER).get());
        portalUser.setRoles(roles);
        userRepository.save(portalUser);

        return Optional.ofNullable(portalUser);
    }

    @Override
    public PortalUserDto toPortalUserDto(PortalUser portalUser) {
        PortalUserDto dto = new PortalUserDto(portalUser.getId(), portalUser.getName(), portalUser.getEmail(), portalUser.getPhoneNumber());
        return dto;
    }

    @Override
    public void createPasswordResetTokenForUser(PortalUser portalUser) {
        String token = UUID.randomUUID().toString();
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetToken.setExpiryDate(LocalDateTime.now().plusHours(1));
        passwordResetToken.setPortalUser(portalUser);
        passwordResetToken.setToken(token);
        passwordResetRepository.save(passwordResetToken);
        mailService.sendPasswordResetEmail(passwordResetToken);
    }

    @Override
    public void updateUserPassword(PortalUser portalUser, String password) {
        portalUser.setPassword(encoder.encode(password));
        userRepository.save(portalUser);
    }

}
