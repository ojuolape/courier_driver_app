package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.message.request.DriverVehicleForm;
import com.nt.courierapp.message.response.DriverDto;
import com.nt.courierapp.message.response.DriverVehicleDto;
import com.nt.courierapp.model.*;
import com.nt.courierapp.repository.DriverRepository;
import com.nt.courierapp.repository.VehicleDriverRepository;
import com.nt.courierapp.repository.VehicleRepository;
import com.nt.courierapp.service.DriverService;
import com.nt.courierapp.service.UserService;
import com.nt.courierapp.service.VehicleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    UserService userService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    VehicleDriverRepository vehicleDriverRepository;


    @Override
    @Transactional
    public Driver createDriver(DriverVehicleForm driverVehicleForm) {
        if(StringUtils.isNotBlank(driverVehicleForm.getEmailAddress())) {
            if(driverRepository.findByEmailAddress(driverVehicleForm.getEmailAddress()).isPresent()){
               throw new IllegalArgumentException("Driver with email address already exists");
            };
        }
        if(driverRepository.findByPhoneNumber(driverVehicleForm.getPhoneNumber()).isPresent()){
            throw new IllegalArgumentException("Driver with mobile number already exists");
        };
        String vehicleNumber = driverVehicleForm.getVehicleNumber().replaceAll(" ", "");

        Vehicle vehicle = vehicleRepository.findByVehicleNumber(vehicleNumber).orElseThrow(() -> new IllegalArgumentException(String.format("Vehicle with number '%s' does not exist", vehicleNumber)));

        Driver driver = new Driver();
        driver.setCreatedBy(userService.getLoggedInUser().orElseThrow(()-> new IllegalArgumentException("Unauthenticated")));
        driver.setDateCreated(LocalDateTime.now());
        driver.setEmailAddress(driverVehicleForm.getEmailAddress());
        driver.setPhoneNumber(driverVehicleForm.getPhoneNumber());
        driver.setName(driverVehicleForm.getName());
        PortalUser portalUser = userService.createDriverUser(driver, driverVehicleForm.getUsername()).get();
        driver.setPortalUser(portalUser);
        driverRepository.save(driver);

        vehicleService.addDriverToVehicle(vehicle, driver);
        return driver;
    }

    @Override
    public Page findBySearchFilter(String filter, Pageable pageable) {
        Page page = driverRepository.findAll(new Specification<Driver>() {
            @Override
            public Predicate toPredicate(Root<Driver> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(StringUtils.isNotBlank(filter)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%"+filter.toLowerCase()+"%")));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }

    @Override
    public DriverDto toDriverDto(Driver driver){
        DriverDto dto = new DriverDto();
        dto.setDateCreated(driver.getDateCreated());
        dto.setName(driver.getName());
        dto.setPhoneNumber(driver.getPhoneNumber());
        dto.setEmailAddress(driver.getEmailAddress());

        List<String> vehicles = vehicleDriverRepository.findAllVehiclesByDriver(driver.getId());
        dto.setVehicleNumber(StringUtils.join(vehicles, ", "));
        return dto;
    }
}
