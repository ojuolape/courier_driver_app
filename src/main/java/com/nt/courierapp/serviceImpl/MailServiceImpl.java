package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.model.*;
import com.nt.courierapp.repository.DeliveryRequestRepository;
import com.nt.courierapp.service.AddressService;
import com.nt.courierapp.service.MailService;
import com.nt.courierapp.service.SettingService;
import freemarker.template.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@EnableAsync
@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    private SettingService settingService;

    @Autowired
    private DeliveryRequestRepository deliveryRequestRepository;

    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

    @Override
    public void sendSimpleMail(String to, String subject, String message) {

    }

    @Async
    @Override
    public void sendEmail(String mailBody, String subject, String recipient) {
        String host = settingService.getSettingByName(SettingsNameConstant.SMTP_HOST, "smtpout.asia.secureserver.net");
        String port = settingService.getSettingByName(SettingsNameConstant.SMTP_PORT, "465");
        final String username = settingService.getSettingByName(SettingsNameConstant.EMAIL_USERNAME, "info@courier.com");
        final String password = settingService.getSettingByName(SettingsNameConstant.EMAIL_PASSWORD, "jj");

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("no-reply@naytifbox.com"));

            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            // Put your HTML content using HTML markup
            message.setContent( mailBody , "text/html;charset=utf-8");
            // Send message
            Transport.send(message);
            logger.info("Sent message successfully....");

        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public String getContentFromTemplate(Map<String, Object> model, String templateName) {
        StringBuffer content = new StringBuffer();

        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfig.getTemplate(templateName), model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    @Async
    @Override
    public void sendNewDeliveryRequestMail(DeliveryRequest deliveryRequest) {
       try {
       sendNewDeliveryRequestMailToAdmin(deliveryRequest);
        if(StringUtils.isBlank(deliveryRequest.getCustomerEmail())){
            return;
        }
        
            Map<String, Object> param = new HashMap<>();
            param.put("trackingNumber", deliveryRequest.getTrackingId());
            param.put("pickUpAddress", deliveryRequest.getPickUpAddress().getStreetAddress());
            param.put("destAddress", deliveryRequest.getDeliveryAddress().getStreetAddress());
            param.put("link", String.format("%s/order/%s", settingService.getSettingByName(SettingsNameConstant.HOST_URL, "www.naytifbox.com"), deliveryRequest.getTrackingId()));
            String mailBody = getContentFromTemplate(param, "new-delivery-request.ftl.html");
            sendEmail(mailBody, "Delivery Request from NaytifBox", deliveryRequest.getCustomerEmail());
            
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Async
    @Override
    public void sendNewUserMail(PortalUser portalUser, String password) {
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("name", portalUser.getName());
            param.put("role", portalUser.getRoles().iterator().next().getName());
            param.put("username", portalUser.getUsername());
            param.put("password", password);
            param.put("link", String.format("%s/login", settingService.getSettingByName(SettingsNameConstant.HOST_URL, "www.naytifbox.com")) );
            String mailBody = getContentFromTemplate(param, "new-user.ftl.html");
            sendEmail(mailBody, "Welcome to NaytifBox", portalUser.getEmail());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Async
    @Override
    public void sendPasswordResetEmail(PasswordResetToken passwordResetToken) {
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("link", String.format("%s/reset-password?pwt=%s", settingService.getSettingByName(SettingsNameConstant.HOST_URL, "www.naytifbox.com"), passwordResetToken.getToken()) );
            String mailBody = getContentFromTemplate(param, "password-reset.ftl.html");
            sendEmail(mailBody, "Password Reset", passwordResetToken.getPortalUser().getEmail());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Async
    @Override
    public void sendNewDeliveryRequestMailToAdmin(DeliveryRequest deliveryRequest) {
        try {
            Map<String, Object> param = new HashMap<>();
            param.put("trackingNumber", deliveryRequest.getTrackingId());
            param.put("customerName", deliveryRequest.getCustomerName());
            param.put("customerPhoneNo", deliveryRequest.getCustomerPhoneNo());
            param.put("pickUpAddress", deliveryRequest.getPickUpAddress().getStreetAddress());
            param.put("destAddress", deliveryRequest.getDeliveryAddress().getStreetAddress());
            long pending = deliveryRequestRepository.countAllByOrderStatus(DeliveryStatusConstant.PENDING).orElse(0L);
            param.put("totalPending",pending );
            String mailBody = getContentFromTemplate(param, "new-delivery-request-admin.ftl.html");
            sendEmail(mailBody, "New Delivery Request from NaytifBox", settingService.getSettingByName(SettingsNameConstant.NEW_DELIVERY_REQUEST_NOTIFICATION_RECIPIENT, "admin@naytifbox.com"));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
