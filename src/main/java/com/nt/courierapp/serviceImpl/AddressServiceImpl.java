package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.model.Address;
import com.nt.courierapp.repository.AddressRepository;
import com.nt.courierapp.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    AddressRepository addressRepository;

    @Override
    @Transactional
    public Optional<Address> createAddress(AddressForm addressForm) {
        Address address = new Address();
        address.setStreetAddress(addressForm.getStreetAddress());
        address.setLatitude(addressForm.getLatitude());
        address.setLongitude(addressForm.getLongitude());
        addressRepository.save(address);
        return Optional.ofNullable(address);
    }
}
