package com.nt.courierapp.serviceImpl;

import com.google.gson.Gson;
import com.nt.courierapp.message.dto.google_gps.Distance;
import com.nt.courierapp.message.dto.google_gps.GpsDistanceDto;
import com.nt.courierapp.message.request.AddressForm;
import com.nt.courierapp.message.request.LoginForm;
import com.nt.courierapp.message.request.TraileLoginResponse;
import com.nt.courierapp.message.request.order.OrderForm;
import com.nt.courierapp.message.request.order.VendorOrderForm;
import com.nt.courierapp.message.response.order.DeliveryRequestDto;
import com.nt.courierapp.message.response.order.OrderDistanceDto;
import com.nt.courierapp.model.*;
import com.nt.courierapp.repository.DeliveryRequestRepository;
import com.nt.courierapp.repository.DeliveryHistoryRepository;
import com.nt.courierapp.repository.SettingsRepository;
import com.nt.courierapp.service.*;
import com.nt.courierapp.service.qualifier.TrackingIdSequence;
import com.nt.courierapp.service.sequence.SequenceService;
import com.nt.courierapp.utils.WebUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@EnableAsync
@Service
public class OrderServiceImpl implements OrderService{
    @Autowired
    private SettingService settingsService;

    @Autowired
    @TrackingIdSequence
    private SequenceService trackingIdSeq;
    @Autowired
    DeliveryRequestRepository deliveryRequestRepository;
    @Autowired
    UserService userService;
    @Autowired
    AddressService addressService;
    @Autowired
    DeliveryHistoryRepository deliveryHistoryRepository;
    @Autowired
    DriverService driverService;

    @Autowired
    MailService mailService;

    @Autowired
    TraileApiClient traileApiClient;


    @Override
    public OrderDistanceDto calculateOrderDistanceAndAmount(String originLatLng, String destLatLng) {
        OrderDistanceDto dto ;
        try{
            OkHttpClient okHttpClient = WebUtils.getUnsafeOkHttpClient();
            String url2 = String.format("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=%s&destinations=%s&key=AIzaSyDs7r-H-p91b7yOY9SLKJpRIFol7oDGi6U", originLatLng, destLatLng);
           // String url = String.format("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&key=AIzaSyDs7r-H-p91b7yOY9SLKJpRIFol7oDGi6U&origin=%s&destination=%s",originLatLng, destLatLng);
            System.out.println(url2);
            Request request = new Request.Builder()
                    .url(url2)
                    .get()
                    .build();

            Response response;
            response = okHttpClient.newCall(request).execute();

            if(response.isSuccessful()) {
                String responseData = response.body().string();
                GpsDistanceDto gpsDistanceDto = new Gson().fromJson(responseData, GpsDistanceDto.class);
                if(gpsDistanceDto != null){
                    Distance distance = gpsDistanceDto.getRows()[0].getElements()[0].getDistance();
                    dto = new OrderDistanceDto(distance.getText(), calculateOrderAmountByDistanceInMetres(distance.getValue()));
                    return dto;
                }


            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Long calculateOrderAmountByDistanceInMetres(Long metres) {
        Optional<String> settings = settingsService.getSettingByName(SettingsNameConstant.DEFAULT_PRICE_PER_KILOMETER_IN_KOBO);
        if(!settings.isPresent()){
            return null;
        }

        Long amount = new BigDecimal(settings.get()).multiply(BigDecimal.valueOf(metres)).multiply(BigDecimal.valueOf(100L)).longValue();
        return amount;
    }

    @Override
    @Transactional
    public DeliveryRequest createOrder(OrderForm orderForm) {
        DeliveryRequest deliveryRequest = new DeliveryRequest();
        deliveryRequest.setEstimatedAmount(orderForm.getEstimatedAmount());
        deliveryRequest.setCreatedBy(userService.getLoggedInUser().orElse(null));
        deliveryRequest.setCustomerName(orderForm.getCustomerName());
        deliveryRequest.setCustomerPhoneNo(orderForm.getCustomerPhoneNo());
        deliveryRequest.setCustomerEmail(orderForm.getCustomerEmail());
        deliveryRequest.setDateCreated(LocalDateTime.now());
        deliveryRequest.setRecipientName(orderForm.getRecipientName());
        deliveryRequest.setRecipientPhoneNo(orderForm.getRecipientPhoneNo());
        deliveryRequest.setDeliveryItem(orderForm.getDeliveryItem());
        deliveryRequest.setTrackingId(String.format("TR%04d",trackingIdSeq.getNextId()));
        deliveryRequest.setOrderStatus(DeliveryStatusConstant.PENDING);
        deliveryRequest.setDeliveryType(orderForm.getDeliveryType());

        Address destAddress = addressService.createAddress(orderForm.getDeliveryAddress()).orElseThrow(() -> new RuntimeException("Unable to create destination address"));
        Address pickup_address = addressService.createAddress(orderForm.getPickUpAddress()).orElseThrow(() -> new RuntimeException("Unable to create pickup address"));

        deliveryRequest.setPickUpAddress(pickup_address);
        deliveryRequest.setDeliveryAddress(destAddress);
        deliveryRequestRepository.save(deliveryRequest);

        updateOrderHistory(deliveryRequest, deliveryRequest.getOrderStatus(), deliveryRequest.getCreatedBy());
        mailService.sendNewDeliveryRequestMail(deliveryRequest);

        return deliveryRequest;
    }

    @Override
    @Transactional
    public DeliveryRequest createVendorOrder(VendorOrderForm orderForm) {
        DeliveryRequest deliveryRequest = new DeliveryRequest();
        deliveryRequest.setEstimatedAmount(orderForm.getEstimatedAmount());
        PortalUser portalUser = userService.getLoggedInUser().orElseThrow(
                () -> new IllegalArgumentException("User not logged in")
        );
        deliveryRequest.setCreatedBy(portalUser);
        deliveryRequest.setCustomerName(portalUser.getName());
        deliveryRequest.setCustomerPhoneNo(portalUser.getPhoneNumber());
        deliveryRequest.setCustomerEmail(portalUser.getEmail());
        deliveryRequest.setDateCreated(LocalDateTime.now());
        deliveryRequest.setRecipientName(orderForm.getRecipientName());
        deliveryRequest.setRecipientPhoneNo(orderForm.getRecipientPhoneNo());
        deliveryRequest.setDeliveryItem(orderForm.getDeliveryItem());
        deliveryRequest.setTrackingId(String.format("TR%04d",trackingIdSeq.getNextId()));
        deliveryRequest.setOrderStatus(DeliveryStatusConstant.PENDING);
        deliveryRequest.setDeliveryType(orderForm.getDeliveryType());

        Address destAddress = addressService.createAddress(orderForm.getDeliveryAddress()).orElseThrow(() -> new RuntimeException("Unable to create destination address"));
        Address pickup_address = addressService.createAddress(orderForm.getPickUpAddress()).orElseThrow(() -> new RuntimeException("Unable to create pickup address"));

        deliveryRequest.setPickUpAddress(pickup_address);
        deliveryRequest.setDeliveryAddress(destAddress);
        deliveryRequestRepository.save(deliveryRequest);

        updateOrderHistory(deliveryRequest, deliveryRequest.getOrderStatus(), deliveryRequest.getCreatedBy());
        mailService.sendNewDeliveryRequestMail(deliveryRequest);

        return deliveryRequest;
    }
    @Override
    @Transactional
    public Optional<DeliveryHistory> updateOrderHistory(DeliveryRequest deliveryRequest, DeliveryStatusConstant deliveryStatusConstant, PortalUser updatedBy) {
        deliveryRequest.setOrderStatus(deliveryStatusConstant);
        deliveryRequestRepository.save(deliveryRequest);

        Optional<DeliveryHistory> orderStatusHistory = deliveryHistoryRepository.findByDeliveryRequestAndStatus(deliveryRequest, deliveryStatusConstant);
        if(orderStatusHistory.isPresent()){
            DeliveryHistory deliveryHistory = orderStatusHistory.get();
            deliveryHistory.setDateCreated(LocalDateTime.now());
            return Optional.ofNullable(deliveryHistoryRepository.save(deliveryHistory));
        }
        DeliveryHistory orderHistory = new DeliveryHistory();
        orderHistory.setCreatedBy(updatedBy);
        orderHistory.setDateCreated(LocalDateTime.now());
        orderHistory.setDeliveryRequest(deliveryRequest);
        orderHistory.setStatus(deliveryStatusConstant);
        deliveryHistoryRepository.save(orderHistory);
        return Optional.ofNullable(orderHistory);
    }

    @Override
    public DeliveryRequestDto toDeliveryRequestDto(DeliveryRequest deliveryRequest) {
        DeliveryRequestDto dto = new DeliveryRequestDto(deliveryRequest);
        if(deliveryRequest.getCreatedBy() != null) {
            dto.setCreatedBy(userService.toPortalUserDto(deliveryRequest.getCreatedBy()));
        }
        if(deliveryRequest.getDriver() != null){
            dto.setDriver(driverService.toDriverDto(deliveryRequest.getDriver()));
        }
        dto.setPickUpAddress(new AddressForm(deliveryRequest.getPickUpAddress()));
        dto.setDeliveryAddress(new AddressForm(deliveryRequest.getDeliveryAddress()));
        dto.setDeliveryHistory(deliveryHistoryRepository.findAllByDeliveryRequest(deliveryRequest));
        return dto;
    }

    @Override
    @Transactional
    public DeliveryRequest addDriverToDeliveryRequest(DeliveryRequest deliveryRequest, Driver driver) {
        deliveryRequest.setDriver(driver);
        deliveryRequest.setLastUpdated(LocalDateTime.now());
        deliveryRequest.setOrderStatus(DeliveryStatusConstant.DRIVER_ASSIGNED);
        deliveryRequestRepository.save(deliveryRequest);

        updateOrderHistory(deliveryRequest, DeliveryStatusConstant.DRIVER_ASSIGNED, userService.getLoggedInUser().get());
        sendTaskToDrivers(deliveryRequest);
        return deliveryRequest;
    }

    @Override
    public DeliveryRequest updateDeliveryAmount(DeliveryRequest deliveryRequest, BigDecimal amount) {
        deliveryRequest.setAmount(amount.multiply(BigDecimal.valueOf(100L)).longValue());
        deliveryRequestRepository.save(deliveryRequest);
        return deliveryRequest;
    }

    @Override
    public Page findBySearchFilter(String filter, Pageable pageable) {
        Page page = deliveryRequestRepository.findAll(new Specification<DeliveryRequest>() {
            @Override
            public Predicate toPredicate(Root<DeliveryRequest> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if(StringUtils.isNotBlank(filter)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("trackingId")), ""+filter.toLowerCase()+"%")));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }

    @Override
    public Page findBySearchFilter(String filter, Pageable pageable, PortalUser portalUser) {
        Page page = deliveryRequestRepository.findAll(new Specification<DeliveryRequest>() {
            @Override
            public Predicate toPredicate(Root<DeliveryRequest> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if (StringUtils.isNotBlank(filter)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("trackingId")), "" + filter.toLowerCase() + "%")));
                }
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("createdBy"), portalUser)));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }

    @Override
    public Page findByDriverAndSearchFilter(String filter, Pageable pageable, Driver driver) {
        Page page = deliveryRequestRepository.findAll(new Specification<DeliveryRequest>() {
            @Override
            public Predicate toPredicate(Root<DeliveryRequest> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();
                if (StringUtils.isNotBlank(filter)) {
                    predicates.add(criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("trackingId")), "" + filter.toLowerCase() + "%")));
                }
                predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("driver"), driver)));
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, pageable);
        return page;
    }

    @Async
    public void sendTaskToDrivers(DeliveryRequest deliveryRequest){
        try {
            String apiHash = getTraileUserApiHash();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getTraileUserApiHash(){
        String apiHash = settingsService.getSettingByName(SettingsNameConstant.TRAILE_USER_API_HASH).orElse(null);
        if(StringUtils.isNotBlank(apiHash)){
            return apiHash;
        }

        LoginForm loginForm = new LoginForm(settingsService.getSettingByName(SettingsNameConstant.TRAILE_PASSWORD, "abc"),
                settingsService.getSettingByName(SettingsNameConstant.TRAILE_USERNAME, "admin@naytifbox.com"));

        TraileLoginResponse response = traileApiClient.login(loginForm);
        settingsService.getSettingByName(SettingsNameConstant.TRAILE_API_BASE_URL, response.getUser_api_hash());
        return response.getUser_api_hash();
    }

}
