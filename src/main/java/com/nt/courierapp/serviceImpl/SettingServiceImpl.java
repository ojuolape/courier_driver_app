package com.nt.courierapp.serviceImpl;

import com.nt.courierapp.model.Settings;
import com.nt.courierapp.model.SettingsNameConstant;
import com.nt.courierapp.model.SettingsTypeConstant;
import com.nt.courierapp.repository.SettingsRepository;
import com.nt.courierapp.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SettingServiceImpl implements SettingService{

    @Autowired
    private SettingsRepository settingsRepository;

    @Override
    public String getSettingByName(SettingsNameConstant settingsNameConstant, String defaultValue) {
        Optional<Settings> settings = settingsRepository.getSettingsByName(settingsNameConstant);
        if(!settings.isPresent()){
            Settings setting = new Settings();
            setting.setName(settingsNameConstant);
            setting.setType(SettingsTypeConstant.SYSTEM);
            setting.setValue(defaultValue);
            settingsRepository.save(setting);
            return setting.getValue();
        }
        return settings.get().getValue();
    }

    @Override
    public Optional<String> getSettingByName(SettingsNameConstant settingsNameConstant) {
        return settingsRepository.getSettingsValueByName(settingsNameConstant);
    }
}
